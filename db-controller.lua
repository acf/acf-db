local mymodule = {}

mymodule.default_action = "listdatabases"

-- Use acf-db-lib to allow editing of the database
dbcontrollerfunctions = require("dbcontrollerfunctions")
for n,f in pairs(dbcontrollerfunctions) do
	mymodule[n] = f
end

return mymodule
