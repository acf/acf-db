local mymodule = {}

-- Load libraries
db = require("acf.db")
dbmodelfunctions = require("dbmodelfunctions")

for n,f in pairs(dbmodelfunctions) do
	mymodule[n] = function(...)
		return f(db.create, ...)
	end
end

return mymodule
