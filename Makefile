APP_NAME=db
PACKAGE=acf-$(APP_NAME)
VERSION=0.2.1

APP_DIST=db* \

SUBDIRS=lib
EXTRA_DIST=README Makefile config.mk
DISTFILES=$(APP_DIST) $(EXTRA_DIST)

install_dir=$(DESTDIR)/$(appdir)/$(APP_NAME)

phony+=all
all:

phony+=clean
clean:

export DISTDIR DESTDIR
phony+=install-recursive
install-recursive:
	for dir in $(SUBDIRS); do\
		( cd $$dir && $(MAKE) install ) || exit 1;\
	done

phony+=install
install: install-recursive $(SUBDIRS)
	mkdir -p "$(install_dir)"
	cp -a $(APP_DIST) "$(install_dir)"
	for i in $$(ls -1 $(DESTDIR)/$(acflibdir)/db-*.lsp); do\
		ln -sf ../../lib/$$(basename $$i) $(install_dir)/$$(echo "$$(basename $$i)" | sed "s/db/$(APP_NAME)/");\
	done

include config.mk

.PHONY: $(phony)
