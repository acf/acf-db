local mymodule = {}

function mymodule.listdatabases(self)
	return self.model.list_databases(self, self.clientdata)
end

function mymodule.listtables(self)
	return self.model.list_tables(self, self.clientdata)
end

function mymodule.viewtable(self)
	return self.model.list_table_entries(self, self.clientdata)
end

function mymodule.deletetableentry(self)
	return self.handle_form(self, self.model.get_delete_table_entry, self.model.delete_table_entry, self.clientdata, "Delete", "Delete Table Entry", "Table Entry deleted")
end

function mymodule.updatetableentry(self)
	return self.handle_form(self, self.model.get_table_entry, self.model.update_table_entry, self.clientdata, "Update", "Update Table Entry", "Entry updated")
end

function mymodule.createtableentry(self)
	return self.handle_form(self, self.model.get_new_table_entry, self.model.create_table_entry, self.clientdata, "Create", "Create New Table Entry", "Entry created")
end

return mymodule
