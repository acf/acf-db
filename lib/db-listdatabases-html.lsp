<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>
<% db = require("acf.db") %>

<% if form.value.databases and #form.value.databases.value>0 then %>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#listdatabases").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>
<% end %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<% local database %>
<% if form.value.connection then
	-- hide the database, since we don't need it to list databases
	database = form.value.connection.value.database
	form.value.connection.value.database = nil
end %>
<% if form.value.connection and ( next(form.value.connection.value) ~= nil or form.value.connection.errtxt ) then
	htmlviewfunctions.displayformstart(form, page_info)
	htmlviewfunctions.displayitem(form.value.connection, page_info, htmlviewfunctions.incrementheader(header_level), "connection")
	form.option = "Update"
	htmlviewfunctions.displayformend(form, htmlviewfunctions.incrementheader(header_level))
end %>
<% -- We need to pass the connection key values to listtables
local formvalues = {}
if form.value.connection then
	formvalues.connection = form.value.connection
	form.value.connection.value.database = database
	for n,v in pairs(form.value.connection.value) do v.type="hidden" end
end
%>
<% if form.value.databases and #form.value.databases.value>0 then %>
<table id="listdatabases" class="tablesorter"><thead>
        <tr>
		<th>Action</th>
		<th>Database</th>
	</tr>
</thead><tbody>
<% for i,dbase in ipairs(form.value.databases.value) do %>
	<% formvalues.connection.value.database.value = dbase %>
	<tr>
		<td>
		<% if viewlibrary.check_permission("listtables") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value=formvalues, label="", option="View", action="listtables"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(dbase) %></td>
	</tr>
<% end %>
</tbody></table>
<% elseif form.value.connection and form.value.connection.value.engine and tonumber(form.value.connection.value.engine.value) and tonumber(form.value.connection.value.engine.value) == db.engine.sqlite3 then
	form.value.connection.label = "Database"
	form.value.connection.value.database.type = "text"
	htmlviewfunctions.displayitem(cfe({type="link", value=formvalues, label="", option="View", action="listtables"}), page_info, header_level)
else %>
<p>No databases found</p>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
