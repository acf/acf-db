<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% if form.value.tables and #form.value.tables.value>0 then %>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#listtables").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>
<% end %>

<% htmlviewfunctions.displaycommandresults({"createdatabase"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<% htmlviewfunctions.displayinfo(form) %>
<% if form.value.connection and ( next(form.value.connection.value) ~= nil or form.value.connection.errtxt ) then
	htmlviewfunctions.displayformstart(form, page_info)
	htmlviewfunctions.displayitem(form.value.connection, page_info, htmlviewfunctions.incrementheader(header_level), "connection")
	form.option = "Update"
	htmlviewfunctions.displayformend(form, htmlviewfunctions.incrementheader(header_level))
end %>
<% if form.value.tables and #form.value.tables.value>0 then %>
<table id="listtables" class="tablesorter"><thead>
        <tr>
		<th>Action</th>
		<th>Table</th>
	</tr>
</thead><tbody>
<% -- We will reuse the form connection structure to pass key values to viewtable
local formvalues = {}
if form.value.connection then
	formvalues.connection = form.value.connection
	for n,v in pairs(form.value.connection.value) do v.type="hidden" end
end
formvalues.table = cfe({ type="hidden" })
%>
<% for i,tab in ipairs(form.value.tables.value) do %>
	<% formvalues.table.value = tab %>
	<tr>
		<td>
		<% if viewlibrary.check_permission("viewtable") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value=formvalues, label="", option="View", action="viewtable"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(tab) %></td>
	</tr>
<% end %>
</tbody></table>
<% else %>
<p>No tables found</p>
<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createdatabase") then
        viewlibrary.dispatch_component("createdatabase")
end %>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
